<?php
    include('conn.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hajiku</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="./assets/css/countdown.css">
</head>
<body>
<!-- Demo header-->
<section class="header text-center">
    <div class="container text-white">
        <header class="py-5">
            <h1 class="display-4">Hajiku</h1>
            <p class="font-italic mb-1">Layanan Haji dan Umroh</p>
        </header>
    </div>
</section>


<!-- Sticky navbar-->
<header class="header sticky-top">
    <nav class="navbar navbar-expand-lg navbar-light bg-white py-3 shadow-sm">
        <div class="container"><a class="navbar-brand" href="#">
            <strong class="h6 mb-0 font-weight-bold text-uppercase">Hajiku</strong></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a class="nav-link" href="index.php">Paket Haji</a></li>
                    <li class="nav-item active"><a class="nav-link" href="tambah_paket.php">Tambah Paket</a></li>
                    <li class="nav-item active"><a class="nav-link" href="data_pesanan.php">Data Pesanan</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<!-- Demo content-->
<section class="py-5 section-1">
    <div class="row">
        <div class="col-lg-3">
          <div class="rounded bg-gradient-2 text-white shadow-sm p-3 text-center ml-2 mb-5" >
              <p class="mb-0 font-weight-bold text-uppercase">Waktu Terbatas!</p>
              <div id="clock-a" class="countdown pt-4" ></div>
          </div>
        </div>
        
        <div class="col-lg-9">
            <div class="row">
                <div class="col-6">
                    <h2 class="font-weight-bold mb-3">Layanan Kami</h2>
                </div>
                <div class="col-5 mr-2">
                    <form action="index.php" method="get"> 
                        <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4">
                            <div class="input-group">
                              <input type="text" name="cari" placeholder="What're you searching for?" aria-describedby="button-addon1" class="form-control border-0 bg-light">
                              <div class="input-group-append">
                                <button id="button-addon1" type="submit" class="btn btn-link text-primary"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </div>
                    </form>
                    <?php 
                        if(isset($_GET['cari'])){
                          $cari = $_GET['cari'];
                        }
                    ?>
                </div>
            </div>
            <div class="row pb-5 mb-4 mr-2">
                <?php 
                  if(isset($_GET['cari'])){
                    $cari = $_GET['cari'];
                    $data = mysqli_query($koneksi, "SELECT * FROM paket_haji where nama_paket like '%".$cari."%'");       
                  }else{
                    $data = mysqli_query($koneksi, "SELECT * FROM paket_haji");   
                  }
                  while($d = mysqli_fetch_array($data)){
                  ?>
                <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
                    <div class="card rounded shadow-sm border-0">
                        <div class="card-body p-4"><img src="<?="file/".$d['foto'];?>" alt="" style="max-height: 100px" class="img-fluid d-block mx-auto mb-3">
                            <h5> <a href="#" class="text-dark"><?= $d['nama_paket']?></a></h5>
                            <p class="small text-muted font-italic"><?= $d['deskripsi']?></p>
                            <h6>Rp <?=$d['harga']?></h6>
                            <p class="small text-muted font-italic">Potongan Rp 1.000.000</p>
                            <button type="button" class="btn btn-primary btn-daftar" data-harga="<?=$d['harga']?>" data-toggle="modal" data-id="<?= $d['id']?>" data-target="#daftarModal">Ambil Paket</button>

                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="daftarModal" tabindex="-1" role="dialog" aria-labelledby="daftarModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="daftarModalLabel">Daftar Paket</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="proses_daftar.php" method="POST" >
            <div class="form-group">
            <!-- <label for="recipient-name" class="col-form-label">id haji </label> -->
            <input type="hidden" class="form-control" name="id_haji" id="id_haji">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nama </label>
            <input type="text" class="form-control" onkeypress="return huruf(event)" name="nama" id="nama">
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Usia </label>
            <input type="text" onkeypress="return angka(event)" class="form-control" name="usia" id="usia">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Alamat </label>
            <textarea class="form-control" onkeypress="return huruf(event)" name="alamat" id="alamat"></textarea>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Total Harga </label>
            <input type="text" class="form-control" id="total" disabled>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Daftar</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <!-- Footer -->
  <footer class="bg-white">
    <div class="container py-5">
      <div class="row py-4">
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0"><img src="img/logo.png" alt="" width="180" class="mb-3">
          <p class="font-italic text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
          <ul class="list-inline mt-4">
            <li class="list-inline-item"><a href="#" target="_blank" title="twitter"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="facebook"><i class="fa fa-facebook"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="instagram"><i class="fa fa-instagram"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="pinterest"><i class="fa fa-pinterest"></i></a></li>
            <li class="list-inline-item"><a href="#" target="_blank" title="vimeo"><i class="fa fa-vimeo"></i></a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
          <h6 class="text-uppercase font-weight-bold mb-4">Shop</h6>
          <ul class="list-unstyled mb-0">
            <li class="mb-2"><a href="#" class="text-muted">For Women</a></li>
            <li class="mb-2"><a href="#" class="text-muted">For Men</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Stores</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Our Blog</a></li>
          </ul>
        </div>
        <div class="col-lg-2 col-md-6 mb-4 mb-lg-0">
          <h6 class="text-uppercase font-weight-bold mb-4">Company</h6>
          <ul class="list-unstyled mb-0">
            <li class="mb-2"><a href="#" class="text-muted">Login</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Register</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Wishlist</a></li>
            <li class="mb-2"><a href="#" class="text-muted">Our Products</a></li>
          </ul>
        </div>
        <div class="col-lg-4 col-md-6 mb-lg-0">
          <h6 class="text-uppercase font-weight-bold mb-4">Newsletter</h6>
          <p class="text-muted mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At itaque temporibus.</p>
          <div class="p-1 rounded border">
            <div class="input-group">
              <input type="email" placeholder="Enter your email address" aria-describedby="button-addon1" class="form-control border-0 shadow-0">
              <div class="input-group-append">
                <button id="button-addon1" type="submit" class="btn btn-link"><i class="fa fa-paper-plane"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Copyrights -->
    <div class="bg-light py-4">
      <div class="container text-center">
        <p class="text-muted mb-0 py-2">© 2019 Bootstrapious All rights reserved.</p>
      </div>
    </div>
  </footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>
<script src="./assets/countdown.js"></script>
<script type="text/javascript">
    $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus');
    });

    $('.btn-daftar').click(function(){
        var potongan = 1000000;
        var harga = $(this).data('harga');
        var id_haji = $(this).data('id');
        var total = (parseInt(harga) - potongan);
        $('#id_haji').val(id_haji);
        $('#total').val(total);
    });
</script>
<script type="text/javascript">
    function angka(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 48 || charCode > 57)&&charCode>32){
            alert("Masukkan umur anda");
            return false;
        }
        return true;
    }
        
    function huruf(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32) {
            alert("Masukkan hanya berupa huruf");
            return false;
        }
        return true;
    }
</script>
</body>
</html>